class BaseComponent {
  constructor(props, node) {
    this.props = props;
    this.node = node;
  }
  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentWillMount() {}
  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentWillReceiveProps(nextProps) {}
  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentWillUpdate() {}

  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentDidUpdate(prevProps) {}

  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentDidMount() {}

  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentShouldUpdate(nextProps) {
    return true;
  }

  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  componentWillUnmount() {}

  /*
   * standart method of BaseComponent.
   * can be overrided.
   */
  render() {
    throw new Error('obligatory to implementation');
  }
}

module.exports = BaseComponent;
