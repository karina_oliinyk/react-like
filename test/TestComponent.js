/* eslint-disable class-methods-use-this */
const BaseComponent = require('../src/core');

const mocks = {
  componentWillMount: jest.fn(function() { return this.props; }),
  componentWillReceiveProps: jest.fn(function() { return this.props; }),
  componentWillUpdate: jest.fn(function() { return this.props; }),
  componentDidUpdate: jest.fn(function() { return this.props; }),
  componentDidMount: jest.fn(function() { return this.props; }),
  componentShouldUpdate: jest.fn(function() { return true; }),
  componentWillUnmount: jest.fn(function() { return this.props; }),
  render: jest.fn(function() { return this.props; })
};

class TestComponent extends BaseComponent {
  constructor(props, node) {
    Object.values(mocks).forEach(mockFn => mockFn.mockClear());
    super(props, node);
    this.mocks = mocks;
  }

  componentWillMount() {
    mocks.componentWillMount.call(this);
  }

  componentWillReceiveProps(nextProps) {
    mocks.componentWillReceiveProps.call(this, nextProps);
  }

  componentWillUpdate() {
    mocks.componentWillUpdate.call(this);
  }

  componentDidUpdate(prevProps) {
    mocks.componentDidUpdate.call(this, prevProps);
  }

  componentDidMount() {
    mocks.componentDidMount.call(this);
  }

  componentShouldUpdate(nextProps) {
    return mocks.componentShouldUpdate(nextProps);
  }

  componentWillUnmount() {
    mocks.componentWillUnmount.call(this);
  }

  render() {
    mocks.render.call(this);
    return null;
  }
}

module.exports = TestComponent;
