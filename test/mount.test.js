/* eslint-disable no-console */
const TestComponent = require('./TestComponent');

const node = document.createElement('div');
const props = {
  name: 'Ann'
};

describe('Test cases for mount lifecycle', () => {
  const inst = new TestComponent(props, node);
  describe('Test cases for methods', () => {
    test('1) componentWillMount should provide 0 arguments', () => {
      expect(inst.mocks.componentWillMount.mock.calls[0]).toHaveLength(0);
    });
    test('1.1) componentWillMount should have old props in this', () => {
      expect(inst.mocks.componentWillMount).toReturnWith(props);
    });

    test('2) render should provide 0 arguments', () => {
      expect(inst.mocks.render.mock.calls[0]).toHaveLength(0);
    });
    test('2.1) render should have actual props in this', () => {
      expect(inst.mocks.render).toReturnWith(props);
    });

    test('3) componentDidMount should take 0 arguments', () => {
      expect(inst.mocks.componentDidMount.mock.calls[0]).toHaveLength(0);
    });
    test('3.1) componentDidMount should have actual props in this', () => {
      expect(inst.mocks.componentDidMount).toReturnWith(props);
    });
  });

  describe('Test cases for order in lifecycle', () => {
    test('tests for order in lifecycle', () => {
      const inst = new TestComponent(props, node);
      expect(inst.mocks.componentWillMount).toHaveBeenCalledTimes(1);
      expect(inst.mocks.render).toHaveBeenCalledTimes(1);
      expect(inst.mocks.componentDidMount).toHaveBeenCalledTimes(1);

      expect(inst.mocks.componentWillReceiveProps).not.toHaveBeenCalled();
      expect(inst.mocks.componentShouldUpdate).not.toHaveBeenCalled();
      expect(inst.mocks.componentWillUpdate).not.toHaveBeenCalled();
      expect(inst.mocks.componentDidUpdate).not.toHaveBeenCalled();
      expect(inst.mocks.componentWillUnmount).not.toHaveBeenCalled();

      expect(inst.mocks.componentWillMount.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.render.mock.invocationCallOrder[0]);
      expect(inst.mocks.render.mock.invocationCallOrder[0])
        .toBeLessThan(inst.mocks.componentDidMount.mock.invocationCallOrder[0]);
    });
  });

  describe('contains only props and node', () => {
    test('instance contains only props and node', () => {
      const inst = new TestComponent(props, node);
      delete inst.mocks;
      expect(Object.keys(inst).sort()).toEqual(['node', 'props'].sort());
    });
  });
});

describe('Test cases for incoming properties', () => {
  const incomigProps = {
    name: 'Jhon',
    getAge: () => 10,
    children: {},
    pets: null
  };

  const inst = new TestComponent(props, node);
  test('1) propTypes are checked at mount lifecycle', () => {
    TestComponent.propTypes = {
      name: 'string',
      getAge: 'number',
      children: 'Array<string>',
      pets: null,
      newprop: 'string'
    };

    console.warn = jest.fn();
    new TestComponent(incomigProps, node); // eslint-disable-line no-new
    expect(console.warn).toHaveBeenCalledTimes(3);
  });

  test('2) propTypes are checked at update lifecycle', () => {
    TestComponent.propTypes = {
      name: 'Array<string>',
      getAge: 'function',
      children: 'object',
      pets: null
    };

    console.warn = jest.fn();
    inst.props = incomigProps;
    expect(console.warn).toHaveBeenCalledTimes(1);
  });
});
